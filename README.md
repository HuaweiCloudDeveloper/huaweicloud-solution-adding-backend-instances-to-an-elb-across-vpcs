[TOC]

**解决方案介绍**
===============
该解决方案基于独享型弹性负载均衡 ELB的跨VPC后端功能，帮助用户快速实现云上跨VPC添加实例至弹性负载均衡 ELB。适用于用户在云上多个VPC有多台服务器，根据业务诉求灵活配置场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/adding-backend-instances-to-an-elb-across-vpcs.html

**架构图**
---------------

![方案架构](./document/adding-backend-instances-to-an-elb-across-vpcs.png)

**架构描述**
---------------

该解决方案会部署如下资源：
- 创建弹性云服务器ECS，用于部署业务。
- 创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。
- 创建弹性负载均衡ELB，帮助用户根据业务诉求灵活配置，将流量请求转发到云上不同VPC的服务器上。


**组织结构**
---------------

``` lua
huaweicloud-solution-adding-backend-instances-to-an-elb-across-vpcs
├── adding-backend-instances-to-an-elb-across-vpcs.tf.json -- 资源编排模板
```
**开始使用**
---------------

1、查看虚拟私有云VPC实例

在[虚拟私有云VPC控制台](https://console.huaweicloud.com/vpc/?region=cn-north-4&locale=zh-cn#/vpc/vpcs/list)，可查看该方案一键生成的VPC和对应的子网/路由表/弹性服务器ECS。

图1 VPC实例
![VPC实例](./document/readme-image-001.PNG)

2、查看弹性负载均衡实例

在[弹性负载均衡控制台](https://console.huaweicloud.com/vpc/?agencyId=21c19ac150bf4867a8302133acfa94ec&region=cn-north-4&locale=zh-cn#/elb/list)，查看该方案一键部署创建的弹性负载均衡器实例。

图2 弹性负载均衡器实例
![弹性负载均衡器实例](./document/readme-image-002.PNG)


3、查看弹性云服务器实例

在[弹性云服务器控制台](https://console.huaweicloud.com/ecm/?region=cn-north-4#/ecs/manager/vmList)，查看该方案一键部署创建的弹性云服务器实例。

图3 弹性云服务器实例
![弹性云服务器实例](./document/readme-image-003.PNG)


4、查看弹性负载均衡器后端服务器组的跨VPC后端

在[弹性负载均衡器控制台](https://console.huaweicloud.com/ecm/?region=cn-north-4#/ecs/manager/vmList)，查看后端服务器组的详细信息。

图4 跨VPC后端
![跨VPO后端](./document/readme-image-004.PNG)

5、验证跨VPC后端添加实例是否成功(部署Nginx服务为例)

使用浏览器访问绑定了ELB的公网ip，显示如下页面，说明本次访问请求被ELB转发到跨VPC的后端服务器上。

图5 验证跨VPC添加后端服务器成功

![验证跨VPC添加后端服务器成功](./document/readme-image-005.PNG)





